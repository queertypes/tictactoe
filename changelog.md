# 1.1.0 (Sep. 15, 2015)

* Add Haskeline support, including completions

# 1.0.0 (Sep. 11, 2016)

* Core implemented
* Text-based, print to terminal, tested
