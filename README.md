# TicTocToe

Just an implementation of TicTacToe in Haskell.

Compile:

```
$ stack build
```

Test:

```
$ stack test
```

Play:

```
$ stack ghci
> run
```

Enjoy!

```
 O | X | O
 ---------
 X |   | X
 ---------
 O | X | O
 ```