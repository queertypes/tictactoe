{-# LANGUAGE LambdaCase #-}
module TicTacToe.Moves (
  moveOutcome,
  parseMove
) where

import Data.Vector (Vector, (!))
import qualified Data.Vector as V

import TicTacToe.Types


-- |
-- >>> moveOutcome One initBoard
-- Ongoing
-- >>> moveOutcome One (Board (V.fromList [X,X,X,E,E,E,E,E,E]))
-- Win One X
-- >>> moveOutcome One (Board (V.fromList [X,X,O,O,O,X,X,X,O]))
-- Draw
moveOutcome :: Player -> Board -> Outcome
moveOutcome p bd@(Board b)
  | isWin (playerMove p) bd = Win p (playerMove p)
  | isFull b  = Draw
  | otherwise = Ongoing

-- |
-- >>> parseMove "TL"
-- Just TL
-- >>> parseMove "WEE"
-- Nothing
parseMove :: String -> Maybe Pos
parseMove = \case
  "TL" -> Just TL
  "TM" -> Just TM
  "TR" -> Just TR
  "ML" -> Just ML
  "MM" -> Just MM
  "MR" -> Just MR
  "BL" -> Just BL
  "BM" -> Just BM
  "BR" -> Just BR
  _    -> Nothing


--------------------------------------------------------------------------------
winPatterns :: [(Int,Int,Int)]
winPatterns =
  [ (0,1,2), (3,4,5), (6,7,8)
  , (0,3,6), (1,4,7), (2,5,8)
  , (0,4,8), (2,4,6)
  ]

isFull :: Vector Move -> Bool
isFull = not . V.any (== E)

isWin :: Move -> Board -> Bool
isWin mv (Board bd) = go mv bd winPatterns
  where go _ _ [] = False
        go m b ((x,y,z):ms)
          | m == (b ! x) && m == (b ! y) && m == (b ! z) = True
          | otherwise = go m b ms
