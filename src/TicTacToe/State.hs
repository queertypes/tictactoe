module TicTacToe.State (
  nextPlayer,
  updateBoard
) where

import Data.Vector ((!))
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV

import TicTacToe.Types

nextPlayer :: Player -> Player
nextPlayer One = Two
nextPlayer Two = One

updateBoard :: Move -> Pos -> Board -> Maybe Board
updateBoard m p (Board b)
  | bm /= E   = Nothing -- no update
  | otherwise = Just newBoard
  where pos = fromEnum p
        bm  = b ! pos
        newBoard = Board $ (V.modify (\v -> MV.write v pos m)) b
