{-# LANGUAGE TupleSections #-}
module TicTacToe.Settings (
  inputSettings
) where

import System.Console.Haskeline (Completion(..), Settings(..))


inputSettings :: Settings IO
inputSettings = Settings {
    complete = completionF
  , historyFile = Nothing
  , autoAddHistory = False
  }
  where completionF (s,_) = (return . ("",)) $ case s of
          "T" -> completionsTop
          "M" -> completionsMiddle
          "B" -> completionsBottom
          _   -> completionsAll


--------------------------------------------------------------------------------
completionsAll :: [Completion]
completionsAll = map (\s -> Completion s s False) [
  "TL", "TM", "TR", "ML", "MM", "MR", "BL", "BM", "BR"
  ]

completionsTop :: [Completion]
completionsTop = map (\s -> Completion s s False) [
  "TL", "TM", "TR"
  ]

completionsMiddle :: [Completion]
completionsMiddle = map (\s -> Completion s s False) [
  "ML", "MM", "MR"
  ]

completionsBottom :: [Completion]
completionsBottom = map (\s -> Completion s s False) [
  "BL", "BM", "BR"
  ]
