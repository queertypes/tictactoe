{-# LANGUAGE LambdaCase #-}
module TicTacToe.Types (
  Player(..),
  Move(..),
  Outcome(..),
  Pos(..),
  Board(..),

  playerMove,
  initBoard
) where

import Data.Vector (Vector, (!))
import qualified Data.Vector as V

data Player = One | Two
  deriving (Show, Eq)

data Move = X | O | E
  deriving (Eq)

instance Show Move where
  show X = "X"
  show O = "O"
  show E = " "

data Outcome = Win Player Move | Draw | Ongoing
  deriving (Show, Eq)

data Pos = TL | TM | TR | ML | MM | MR | BL | BM | BR
  deriving (Show, Eq, Enum, Bounded)

data Board = Board (Vector Move)
  deriving (Eq)

instance Show Board where
  show = showBoard

{-
Shows the board like:

   | X | O
-----------
 O | X |
-----------
   | O | X
-}
showBoard :: Board -> [Char]
showBoard (Board b) =
  let tl = show $ b ! 0
      tm = show $ b ! 1
      tr = show $ b ! 2
      ml = show $ b ! 3
      mm = show $ b ! 4
      mr = show $ b ! 5
      bl = show $ b ! 6
      bm = show $ b ! 7
      br = show $ b ! 8
  in " " ++ tl ++ " | " ++ tm ++ " | " ++ tr ++ "\n" ++
     "-----------\n"  ++
     " " ++ ml ++ " | " ++ mm ++ " | " ++ mr ++ "\n" ++
     "-----------\n"  ++
     " " ++ bl ++ " | " ++ bm ++ " | " ++ br

playerMove :: Player -> Move
playerMove = \case
  One -> X
  Two -> O

initBoard :: Board
initBoard = Board (V.fromList (replicate 9 E))
