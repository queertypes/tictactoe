module TicTacToe.Game (
  playGame, run
) where

import System.Console.Haskeline
import TicTacToe.Moves
import TicTacToe.Settings
import TicTacToe.State
import TicTacToe.Types

-- To choose a different starting player or starting state
playGame :: Player -> Board -> InputT IO ()
playGame player board = do
  outputStrLn $ "Your Move!! - Player " ++ show player
  outputStrLn (show board)
  newBoard <- playMove player board
  case moveOutcome player newBoard of
    (Win _ _)  -> outputStrLn "You win!"
    Draw       -> outputStrLn "It's a tie."
    Ongoing    -> playGame (nextPlayer player) newBoard

run :: IO ()
run = runInputT inputSettings (playGame One initBoard)


--------------------------------------------------------------------------------
getMove :: Player -> InputT IO Pos
getMove player = do
  moveInput <- getInputLine (show (playerMove player) ++ "> ")
  case parseMove <$> moveInput of
    (Just (Just m)) -> return m
    _ -> outputStrLn ("error: Moves must be one of: " ++ validMoves) >> getMove player
  where validMoves = "TL, TM, TR, ML, MM, MR, BL, BM, BR"



playMove :: Player -> Board -> InputT IO Board
playMove player board = do
  move <- getMove player
  case updateBoard (playerMove player) move board of
    Nothing -> outputStrLn "error: That spot is filled" >> playMove player board
    (Just b) -> return b
